# CONSTANTS FOR A GIVEN SETUP
dvsPythonRepositoryPath = 'E:/DVS/DVS Python/'       # The path in the system to the < DVS Python >
dvsReaderPath           = 'C:/Users/User/Desktop/Data Presentation/repo/' #'E:/Dane/REPOS/DVSReader/'        # The path in the system to the < DVS Reader >

def getFullPathToMappingFile(fName):
    return '/'.join( [dvsPythonRepositoryPath, 'DATA/Electrode Mapping', fName] )
    
#########################################################################################################################################################################################################################################
###                                             EXPORT PARAMETERS
#########################################################################################################################################################################################################################################

inputFilesDir                   = 'E:/DANE/MOPy/MOP_XX_2019_07_16/'  # Relative or absolute path to the SOURCE file (experiment log)
inputFileNames                  = ['04_laser_max_x100_2019_07_16_17_54_43.hdf'] 
outputFilesDir                  = ''                                # Relative or absolute path to the OUTPUT file (experiment log)

electrodeList                   = ['1']                 # Your default range of electrode data to load; 
                                                                    # construction like [str(i) for i in range(n, m + 1)] creates a list with elements ['n', 'n+1',...,'m-1','m']
                                                                    # alternatively you can specify desired electrodes by simply putting their ids into something like this: ['1', '14', '5'] 

timeStart                       = 0                                 # Time (in seconds from the beginning) when read operation should start
timeEnd                         = 1000                              # Time (in seconds from the beginning) when read operation should stop (no need to bother whether acquisition was long enough - this case is handled automatically)

useElectrodeLayoutFile          = False                             # To use or not to use an electrode layout file (whether default or not); If not (False) electrodeList values will be used
customElectrodeLayoutFileName   = ''                                # You can specify custom electrode layout file path (relative or absolute) that can be different from the one saved in source file - used only when different from ''

useElectrodeMappingFile         = True                              # To use or not to use electrode number to ASIC channel mapping; For most of cases it should stay True
customElectrodeMappingFileName  = getFullPathToMappingFile('samtec40_NeuroNexus2xA32_J1_e_ch.map')         # 
                                                                    # You can specify custom electrode mapping file path (relative or absolute) that can be different from the one saved in source file - used only when different from ''

useMatlabWorkspaceCompression   = True                              # Enable MATLAB workspace compression - takes more time to process but may help to save disk space

#########################################################################################################################################################################################################################################

import sys

sys.path.append(dvsReaderPath)

import DVSReader

DVSReader.exportFiles(  inputFilesDir, inputFileNames, outputFilesDir,
                        electrodeList,
                        timeStart, timeEnd,
                        useElectrodeLayoutFile, customElectrodeLayoutFileName,
                        useElectrodeMappingFile, customElectrodeMappingFileName,
                        useMatlabWorkspaceCompression,)
                        
#########################################################################################################################################################################################################################################                        
