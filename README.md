# Steps to follow before using DVSReader

First you need to prepare your system with some software.

## Windows

**HDF5 drivers:**

HDF5 Driver: https://drive.google.com/open?id=16K0txcroSM0SdtRhhwZ9GSADvGpqQ6px
HDF5 Viewer (optional) : https://drive.google.com/open?id=1GneN6w0wWz2LiwUxbEimJW3u-vgYonWL

**Python:**
Python (at least 3.7, Windows x86-64 executable installer) : https://www.python.org/downloads/windows/
You may also install Windows x86 executable installer, however this will vastly limit amount of data that your scripts can operate on (x86-64 is practically limited by the amount of RAM present in your computer).

During installation make sure to check ***Add Python to PATH*** which is unchecked by default.

**Python plugins:**
After Python installation, run Windows PowerShell. In the console type in (and accept with ENTER):

 - `pip install h5py`
 - `pip install matplotlib`
 - `pip install scipy`

h5py, scipy and matplotlib are packages that are required to run `DVSReader`. If you encounter problems at this stage you probably forgot to check ***Add Python to PATH*** during installation process. The easiest way to solve this is to reinstall Python and install packages mentioned above once again.

## Linux - based on Ubuntu 18.04 LTS

On linux you may have Python installed already - just check whether terminal command `python3` works (if not, open terminal and type: `sudo apt install python3`). Then in terminal install aditional packages with exactly the same commands.

# Downloading DVSReader

You can find DVSReader here: https://bitbucket.org/pjurgielewicz/dvsreader 

This repository can be either downloaded *as is* by choosing ***Downloads*** on left hand side and then clicking ***Download repository*** or you can use it as regular ***git*** repository. In the first case you do not need any additional tools but you loose the ease of making upgrades of `DVSReader`. 
If you want to download ***git*** on your computer follow this link: https://git-scm.com/download/win. To learn more about ***git*** and how to use it, this is the place where you can start: https://www.atlassian.com/git/tutorials.

## Using DVSReader

If you want to read acquired data from a data file (SOURCE file) produced by `DVS` you should be interested in making some adjustments inside `DVSReader_client.py`. It is rather straightforward to change a few variables to the user-desired values.

| var | description & info | example value |
|--|--|--|
| `filename` | Path to the SOURCE file (experiment log)| `filename = 'Rat10.dat.0'` |
| `timeStart` | Non-negative real value specifying the beginning of data to read. It is interpreted as number of seconds, counting from the beginning of acquisition| `timeStart = 10.34` |
| `timeEnd` | Non-negative real value and greater than `timeStart` saying when read operation should stop. You do not have to bother whether it is more or less than length of actual data inside - it will be handled automatically by cropping the read range if necessary. It is interpreted as number of seconds, counting from the beginning of acquisition | `timeEnd = 611.4` |
| `electrodeList` | Your default range of electrode data to load. Electrodes' ids should be always positive integer values. For more information about Python lists you can start here: http://effbot.org/zone/python-list.htm | `electrodeList = [i for i in range(1, 10)]`  |
| `useElectrodeLayoutFile` | (True/False) If set to True `DVSReader` will try to find out (basing on the information inside SOURCE file) which and how many electrodes were visible in Visulization during Acquisition - this will discard your default `electrodeList` contents | `useElectrodeLayoutFile = True` |
| `customElectrodeLayoutFileName` | If `useElectrodeLayoutFile = True` and `customElectrodeLayoutFileName` not equal `''` DVSReader will try to use `customElectrodeLayoutFileName` file as the information about the list of electrodes to load|`customElectrodeLayoutFileName = ''` |
| `matlabWorkspaceFileName` | Path to the output Matlab workspace file | `matlabWorkspaceFileName = 'data.mat'`
| `useMatlabWorkspaceCompression` | (`True/False`) Apply compression (or not) on saved Matlab workspace. Enabling takes more time to process but may greatly help to save disk space  | `useMatlabWorkspaceCompression = True`
| `useElectrodeMappingFile` | (True/False) For debugging purposes - in real use-cases should be always set to `True` | `useElectrodeMappingFile = True`
| `customElectrodeMappingFileName` | Custom electrode ID to ASIC channel mapping - used only when `useElectrodeMappingFile = True` and `customElectrodeMappingFileName` not equal `''` - in real use-cases should be always set to `''`| `customElectrodeMappingFileName = '' `

Paths can be either relative to the current working location or absolute.


## Extracted data

When you  have your DVSReader_client.py set up properly, run it:

 - Windows: Open ***PowerShell***, change working directory to the one with `DVSReader` then run: `python .\DVSReader_client.py`
 - Linux: Open terminal, change your working directory to the one with `DVSReader` then run: `python3 DVSReader_client.py`

your Matlab workspace should be saved as `matlabWorkspaceFileName` which you can load directly inside Matlab. Be aware that for big SOURCE files, and big ranges of data to load (in terms of time range as well as number of selected electrodes) this operation will take some time. In extreme situations you may even run out of available RAM - in that case you should consider cutting time range (and/or electrode list items).

### Inner organization of Matlab workspace

When you load `matlabWorkspaceFileName` inside Matlab you will have a new struct variable `DVSData` that contains a bunch of information:
 - `data`: struct of fields of name `e###` that store single electrode data from `timeStart` to `timeEnd`
 - `triggerData`: struct of fields of name `t###` that store single trigger channel data from `timeStart` to `timeEnd`
 - `userNotesData`: cell array keeping information about user-entered notes during acquisition (with writing time) 
 - `metaData`: struct with additional data for example:
	 -  ASIC configuration during acquisition (configuration of filters, DAC bits and so on)
	 - time slice: `tStart = timeStart`, `tEnd = timeEnd`
	 - data rate (`dataRate`) - how many probes of data a single channel gets per one second of acquisition (data frequency)
 - `originalSourceFilePath`, `creatorComputerName`, `creatorName`, `timestamp` - all of these may help to identify which SOURCE file was used, who created this workspace, when and using which machine

All probes of data are ordered naturally with time lapse from `tStart` to `tEnd` and n-th probe in one electrode is always time-synchronized with n-th probe of any other electrode (and trigger channel[s]).

# Contact

For any questions, help, improvement requests write an email: pawel.jurgielewicz[at]agh.edu.pl (the subject should start with [DVSReader])