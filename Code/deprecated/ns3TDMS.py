import sys, struct, os
import numpy as np

class ns3TDMS:
    def __init__(self, fileName, fileAccess, btOrder = sys.byteorder):
        self.fileName           = fileName
        self.fileAccess         = fileAccess
        self.__fileRef          = open(fileName, fileAccess)

        self.__ns3Prop          = { "FILEID" : "NS3_TDMS",
                                    "FILEVER" : [(1, 0)],

                                    "DATATYPES" : {"INTEGER" : {"I8" : [1, np.int8], "I16" : [2, np.int16], "I32" : [3, np.int32], "I64" : [4, np.int64],
                                                                "U8" : [5, np.uint8], "U16" : [6, np.uint16], "U32" : [7, np.uint32] , "U64" : [8, np.uint64]},
                                                   "FP" :       {"FLOAT" : [16, np.float], "DOUBLE" : [17, np.double]},
                                                   "STR" :      {"STRING" : [32]},
                                                   "OTHERS" :   {} ### BINARY OBJECTS INSIDE???
                                                  }

                                  }
        
        self.__byteOrder        = btOrder
        self.__stringEncoding   = "utf-8"
        self.__superheaderLen   = 10
        self.__segmentInfoLen   = 8 * 2

        #it should be holding current segment properties ONLY -> then FLUSH to file
        self.__bufDict          = {}
        self.__bufDataDict      = {}   
        self.__bufStarted       = False
        self.__nextSegmentStartPos = 10

        self.__resetBuffers()

        if fileAccess.lower() == "wb":
            self.WriteFileTypeHeader()
            self.WriteCurrentSegmentSize()
        if fileAccess.lower() == "rb":
            self.__fileRefSize = os.path.getsize(fileName)

        # IT IS MEANT TO STORE TIMED SEGMENT PROPS + DATA INFORMATION (not actual data itself: position in file, type, length)
        self.__timedStructure = []

        #FLATTENING OF DATA TYPES TO HELP FURTHER REVERSE SEARCHES
        self.__flatDataTypes = {}
        for familyType in self.__ns3Prop["DATATYPES"]:
            for specificType in self.__ns3Prop["DATATYPES"][familyType]:
                self.__flatDataTypes[specificType] = self.__ns3Prop["DATATYPES"][familyType][specificType]

        self.__flatDataTypesKeys    = list(self.__flatDataTypes.keys())
        self.__flatDataTypesValues  = [x[0] for x in self.__flatDataTypes.values()]    #list(self.__flatDataTypes.values())

    def __del__(self):
        self.__Close()

    def WriteFileTypeHeader(self):
        self.__fileRef.write( self.__getBytesFromString(self.__ns3Prop["FILEID"]) )
        for i in range(2):
            verID = self.__getBytesFromInt(self.__ns3Prop["FILEVER"][0][i], 1)
            self.__fileRef.write(verID)

    def WriteCurrentSegmentSize(self, segInfoPos = 10, segInfoSize=0, toNextSeg=0):
        self.__fileRef.seek(segInfoPos)

        bToData     = self.__getBytesFromInt(segInfoSize, 8)
        bToNextSeg  = self.__getBytesFromInt(toNextSeg, 8)

        self.__fileRef.write(bToData)
        self.__fileRef.write(bToNextSeg)

        return segInfoPos

    def RegisterProperty(self, name, type, value):

        if name[0] != "/":
            name = "/" + name

        nameSplit = name.split("/")
        lenSplit = len(nameSplit)

        # BEWARE: YOU CAN NOT CREATE PROPERTY AND THE (SUB)GROUP OF THE IDENTICAL NAME IN THE SAME ROOT !!!
        bufDictRef = self.__bufDict[""]
        print(nameSplit, bufDictRef)
        for i in range(1, len(nameSplit)):
            if nameSplit[i] not in bufDictRef:
                bufDictRef.update( {nameSplit[i] : {}} )
            bufDictRef = bufDictRef[nameSplit[i]]

        bufDictRef.update({"prop" : [name, type, value]})  #"prop" key identifies properties (reserved)

        self.__bufStarted = True

    def RegisterDataArray(self, dataArray, dataType):
        self.__bufDataDict["dataArray"] = dataArray
        self.__bufDataDict["dataType"]  = dataType

        self.__bufStarted = True

    def SaveSegment(self, pos):
        self.__fileRef.seek(pos)

        filePosAfterProps   = self.SaveSegmentProperties(pos)
        filePosAfterData    = self.SaveSegmentData(filePosAfterProps)

        print("FILE POSS: ", filePosAfterProps, filePosAfterData)
        ### Update current segment size info
        self.WriteCurrentSegmentSize(pos, filePosAfterProps - pos - self.__segmentInfoLen, filePosAfterData - pos)
        self.__fileRef.seek(filePosAfterData)

        ### Create new dummy segment size info and register its position
        self.WriteCurrentSegmentSize(filePosAfterData)

        #CLEANUP PROPS TREE AND DATA
        self.__resetBuffers()

        self.__nextSegmentStartPos = filePosAfterData

        print("="*10, "SEG SAVED","="*10)

        return filePosAfterData

    def SaveSegmentProperties(self, pos):
        self.__fileRef.seek(pos + self.__segmentInfoLen);
        print("POS BEFORE SAVING SEG PROPS: ", self.__fileRef.tell())
        currentPos = self.__saveSegmentProperties(self.__bufDict[""])
        print("POS AFTER SAVING SEG PROPS: ", currentPos)

        return currentPos
        
    def __saveSegmentProperties(self, lvl):
        props, children = self.__getPropertiesAtCurrentLevel(lvl)
        #print(prop)
        print("Saving ", len(props), " property(ies)...")

        self.__fileRef.write(self.__getBytesFromInt(len(props), 4))

        for d in props:
            for key in d:
                obj = d[key]

                bStr            = self.__getBytesFromString(obj[0])
                bLenStr         = self.__getBytesFromInt(len(bStr), 2)
                bVals, bEnum    = self.ConvertToBytes(obj[1], obj[2])
                bEnum           = self.__getBytesFromInt(bEnum, 1)
                bLenVals        = self.__getBytesFromInt(len(bVals), 4)

                for pItem in [bLenStr, bStr, bEnum, bLenVals, bVals]:
                    self.__fileRef.write(pItem)

                print(obj, bLenStr, bStr, bEnum, bLenVals, bVals)

        ### SAVE PROPS...
        print("Saving ", len(children), " children")
        self.__fileRef.write(self.__getBytesFromInt(len(children), 4))

        for key in children:
            #print(children[key])
            self.__saveSegmentProperties(children[key])

        return self.__fileRef.tell()

    def __getPropertiesAtCurrentLevel(self, lvl):

        propData = []
        children = {}

        for key in lvl:
            if type(lvl[key]) is not dict:
                return

            if self.__isProperty(lvl[key]) == True:
                propData.append( {key : lvl[key]["prop"]} )
            else:
                children[key] = lvl[key]

        return propData, children

    def SaveSegmentData(self, pos):
        self.__fileRef.seek(pos)

        _dt = self.__ns3Prop["DATATYPES"]
        t = self.__bufDataDict["dataType"].upper()

        npType = -1

        if t in _dt["INTEGER"]:
            npType = _dt["INTEGER"][t]
        if t in _dt["FP"]:
            npType = _dt["FP"][t]

        print(t, npType)
        if npType != -1:
            # IF DATA IS A BUFFER (BYTEARRAY?) YOU CAN USE NP.FROMBUFFER()
            npDataArray = np.fromiter(self.__bufDataDict["dataArray"], npType[1], len(self.__bufDataDict["dataArray"]))

            print(self.__bufDataDict["dataArray"])
            print(npDataArray)

            # 1) LEN, 2) TYPE, 3) DATA
            self.__fileRef.write(self.__getBytesFromInt(len(npDataArray), 8))
            self.__fileRef.write(self.__getBytesFromInt(npType[0], 1))
            npDataArray.tofile(self.__fileRef)
        else:
            #Write 0s to description
            self.__fileRef.write(self.__getBytesFromInt(0, 8))
            self.__fileRef.write(self.__getBytesFromInt(0, 1))

        return self.__fileRef.tell()

    ################################################################################################

    def ConvertToBytes(self, type, value):
        _dt = self.__ns3Prop["DATATYPES"]
        t = type.upper()
        if t in _dt["INTEGER"]:
            isSigned = True if t[0] == "I" else False
            bytes = int(t[1:]) // 8
            
            return self.__getBytesFromInt(value, bytes, isSigned), _dt["INTEGER"][t][0]

        if t in _dt["FP"]:
            b = self.__getBytesFromFP(value) if t == "FLOAT" else self.__getBytesFromFP(value, "d")
            return b, _dt["FP"][t][0]

        if t in _dt["STR"]:
            return self.__getBytesFromString(value), _dt["STR"][t][0]

        return "CONVERSION ERROR", value

    def __getBytesFromInt(self, value, width, isSigned = False):
        return value.to_bytes(width, byteorder=self.__byteOrder, signed=isSigned)
    def __getBytesFromString(self, s):
        return s.encode(self.__stringEncoding)
    def __getBytesFromFP(self, value, type = "f"):
        return bytearray(struct.pack(type, value))  

    def __isProperty(self, subTree):
        return True if len(subTree) == 1 and "prop" in subTree else False

    def __resetBuffers(self):
        print("SETTING WRITE BUFFERS TO THEIR DEFAULTS")
        self.__bufDict          = {"" : {}}
        self.__bufDataDict      = {"dataArray" : [],
                                   "dataType" : "I32"}
        self.__bufStarted       = False

    ################################################################################################

    def GetNextSegmentPosition(self):
        return self.__nextSegmentStartPos

    def Close(self):
        if self.__bufStarted:
            s = input("The file is about to close but there are unflushed data in buffers. Do you want to save them? (Y/n): ")
            if s.upper() == "Y":
                print("Flushing buffers ")
                self.SaveSegment(self.__nextSegmentStartPos)
            else:
                print("Aborting data...")

        print("Closing file...")
        self.__fileRef.close()

    ################################################################################################

    def ReadWholeFile(self):
        pos = self.ReadFileType()

        print(pos)
        if pos == -1:
            return

        currSegment = 1
        while pos < self.GetFileSize():
            print("="*10, "READING SEGMENT #", currSegment, "="*10)
            pos = self.ReadSegment(pos)
            currSegment = currSegment + 1

    def ReadFileType(self):
        self.__fileRef.seek(0);
        fileID = self.__getStringFromBytes(self.__fileRef.read(8)) 

        if fileID != self.__ns3Prop["FILEID"]:
            print("Super header incompatible!")
            return -1
        else:
            print("This is %s file"%(self.__ns3Prop["FILEID"]))

        headerVersion = [self.__getIntFromBytes(self.__fileRef.read(1)) for i in range(2)]
        print("Header version: ", headerVersion)

        return self.GetNextSegmentPosition()

    def ReadSegment(self, pos):

        jumps = self.ReadSegmentInfo(pos)
        pos = self.__fileRef.tell()

        segmentDic = {  "jumps" : {"propsBytes" : jumps[0],
                                   "distanceBetweenThisAndNextSegment"   : jumps[1],
                                   "distanceFromSegStartToRawData" : jumps[0] + 9},
                        "props" : self.__bufDict,
                        "dataDesc" : {"dataPos"  : 0,
                                      "dataType" : 0,
                                      "dataElements"  : 0}}

        if pos < self.GetFileSize():
            filePosAfterProps   = self.ReadSegmentProperties(pos)
            filePosAfterData    = self.ReadSegmentData(filePosAfterProps, segmentDic["dataDesc"])

            #print(segmentDic)
            self.__timedStructure.append(segmentDic)
            self.__resetBuffers()
            self.__nextSegmentStartPos = filePosAfterData

            return filePosAfterData

        return pos

    def ReadSegmentInfo(self, pos):
        self.__fileRef.seek(pos)

        jumpToData      = self.__getIntFromBytes(self.__fileRef.read(8)) 
        jumpToNextSeg   = self.__getIntFromBytes(self.__fileRef.read(8)) 

        print("Current segment size: %d bytes\nNext segment after: %d bytes"%(jumpToData, jumpToNextSeg))

        return jumpToData, jumpToNextSeg

    def ReadSegmentProperties(self, pos):
        self.__fileRef.seek(pos)

        return self.__readSegmentProperties();

    def __readSegmentProperties(self):
        nProps = self.__getIntFromBytes(self.__fileRef.read(4)) 
        print("There are %d properties at current lvl"%(nProps))

        for i in range(nProps):
            lenPStr     = self.__getIntFromBytes(self.__fileRef.read(2))           
            pStr        = self.__getStringFromBytes(self.__fileRef.read(lenPStr))    
            pEnum       = self.__getLabelForEnumTypeVal( self.__getIntFromBytes(self.__fileRef.read(1)) )
            lenPVals    = self.__getIntFromBytes(self.__fileRef.read(4))   
            pVals       = self.ConvertFromBytes(pEnum, self.__fileRef.read(lenPVals))

            print([pStr, pEnum, pVals])

            #reuse
            self.RegisterProperty(pStr, pEnum, pVals)

        nKids = self.__getIntFromBytes(self.__fileRef.read(4)) 
        print("There are %d children at current lvl"%(nKids))

        for i in range(nKids):
            self.__readSegmentProperties()

        return self.__fileRef.tell()

    def ReadSegmentData(self, pos, dataDesc):
        self.__fileRef.seek(pos)
        return self.__readSegmentData(dataDesc)

    def __readSegmentData(self, dataDesc):
        dataLen = self.__getIntFromBytes(self.__fileRef.read(8))
        dataType = self.__getLabelForEnumTypeVal(self.__getIntFromBytes(self.__fileRef.read(1)))
        dataArray = np.array([])

        dataDesc["dataPos"] = self.__fileRef.tell()

        _dt = self.__ns3Prop["DATATYPES"]
        t = dataType
        npType = -1

        if t in _dt["INTEGER"]:
            npType = _dt["INTEGER"][t]
        if t in _dt["FP"]:
            npType = _dt["FP"][t]


        # READING ACTUAL DATA IN HERE FOR TESTING PURPOSES ONLY
        # IN PRODUCTION: STORE START, LEN, TYPE TO LOAD FROM FILE WHEN NEEDED
        if npType != -1:
            dataArray = np.fromfile(self.__fileRef, dtype=npType[1], count=dataLen)
            
        dataDesc["dataElements"]    = dataLen
        dataDesc["dataType"]        = dataType

        print("DATA LEN: %d\t DATA TYPE: %s"%(dataLen, dataType))
        print(dataArray)

        return self.__fileRef.tell()

    def ConvertFromBytes(self, type, bytes):
        _dt = self.__ns3Prop["DATATYPES"]
        t = type

        if t in _dt["INTEGER"]:
            return self.__getIntFromBytes(bytes)
        if t in _dt["FP"]:
            return self.__getFPFromBytes(bytes) if t == "FLOAT" else self.__getFPFromBytes(bytes, "d")
        if t in _dt["STR"]:
            return self.__getStringFromBytes(bytes)

        return "ERROR"


    def __getIntFromBytes(self, bytes):
        return int.from_bytes(bytes, byteorder=self.__byteOrder)
    def __getFPFromBytes(self, bytes, type = "f"):
        return struct.unpack(type, bytes)[0]
    def __getStringFromBytes(self, bytes):
        return bytes.decode(self.__stringEncoding)

    def __getLabelForEnumTypeVal(self, enumType):
        return self.__flatDataTypesKeys[self.__flatDataTypesValues.index(enumType)]

    def GetFileSize(self):
        return self.__fileRefSize

    def PrintTimedStructure(self):
        for i in range(len(self.__timedStructure)):
            print("="*20, " #", i+1, "="*20)
            for key in self.__timedStructure[i]:
                print(key, "\n\t", self.__timedStructure[i][key])
            print("\n")


################################################################################################
################################################################################################
################################################################################################

nsObj = ns3TDMS("dane.dat", "wb")

nsObj.RegisterProperty("/Gr1/time", "DOUBLE", 3.141592)
nsObj.RegisterProperty("/Gr1/place/foch", "STRING", "alama kota")
nsObj.RegisterProperty("/Gr1/place/qwk", "U16", 10**4)
nsObj.RegisterProperty("/a", "U32", 12)
nsObj.RegisterDataArray([1, 12, 10000], "U16")
nsObj.SaveSegment(nsObj.GetNextSegmentPosition())

nsObj.RegisterDataArray([1, -1, 113, -5.5 ], "I8")
nsObj.RegisterProperty("/Gr1/time", "DOUBLE", 2.78)
nsObj.SaveSegment(nsObj.GetNextSegmentPosition())

nsObj.RegisterProperty("b", "U8", 111)

nsObj.Close()

################################################################################################
print("#"*50)
################################################################################################

nsRead = ns3TDMS("dane.dat", "rb")
nsRead.ReadWholeFile()

nsRead.PrintTimedStructure()
print(nsRead.GetFileSize())

nsRead.Close()

# TODO:
# 1. Split wisely into classes
# 2. Testing
# 3. Reading only specific segments
# N. Implement reader in LabView...
# infty. Implement writer in LabView