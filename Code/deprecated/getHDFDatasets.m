classdef DVSReader
    %DVSREADERCLASS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = public)
        dataType = 'uint16';
        
        singleChunkSeconds = 0.5;
        singleChunkProbes = 20000;
        datasetName = 'data';
        electrodeMappingAttr = 'Electrode mapping dictionary file';
        mappingDictionariesDir = 'Electrode Mapping';
        
        filename = '';
        outputDataDic = containers.Map('KeyType', 'int32', 'ValueType', 'any')
        mappingDictionary = containers.Map('KeyType', 'uint32', 'ValueType', 'any');      
        
        % MATLAB !!!$*$@#@
        mutableDataDic = containers.Map('KeyType', 'char', 'ValueType', 'any');
        
    end
    
    methods (Access = private)
        function r = buildChunkName(obj, num)
            %%%
            r = strcat('/', num2str(num), '/', obj.datasetName);
        end
        
        function log(obj, argv)
            %%%
            strcat('DVSReader:   ', argv)  
        end
        
        function dataOut = readASICChannel(obj, channel, chunkStartF, chunkEndF, chunkStart, chunkEnd, chunkStartLeft, chunkEndLeft, dataOut)
            %%%
            currentSaveStartPtr = 1; % MATLAB....
            
            %%% First partial chunk
            probesToRead = abs(chunkStartLeft * obj.singleChunkProbes);
            if probesToRead > 0
                chunkName = obj.buildChunkName(chunkStart - 1); 
                r = h5read(obj.filename, chunkName, [obj.singleChunkProbes - probesToRead, channel], [probesToRead, 1]);

                rLen = length(r);
                dataOut(1, currentSaveStartPtr : currentSaveStartPtr + rLen - 1) = r';
                currentSaveStartPtr = currentSaveStartPtr + rLen;
            end

            %%% Full (inside) chunks
            for ch = chunkStart:chunkEnd-1   
                chunkName = obj.buildChunkName(ch);
                r = h5read(obj.filename, chunkName, [1, channel], [obj.singleChunkProbes, 1]);

                rLen = length(r);
                dataOut(1, currentSaveStartPtr : currentSaveStartPtr + rLen - 1) = r';
                currentSaveStartPtr = currentSaveStartPtr + rLen;
            end

            %%% Last partial chunk
            probesToRead = abs(chunkEndLeft * obj.singleChunkProbes);
            if probesToRead > 0 
                if uint64(floor(chunkStartF)) ~= uint64(floor(chunkEndF)) % handle case when start and end fall into the same chunk number
                    chunkName = obj.buildChunkName(chunkEnd);
                    r = h5read(obj.filename, chunkName, [1, channel], [probesToRead, 1]);

                    rLen = length(r);
                    dataOut(1, currentSaveStartPtr : currentSaveStartPtr + rLen - 1) = r';
                    %currentSaveStartPtr = currentSaveStartPtr + rLen;
                else
                    dataOut = dataOut(1, 1:obj.totalArraySize);
                end
            end
            
            % TODO: Handle case with single partial chunk (as in PYTHON)...
            
        end
        
        function buildElectrodeChannelMapping(obj, customElectrodeMappingFileName)
            %%%
            fullAttr = char(h5readatt(obj.filename, strcat('/0/' , obj.datasetName), obj.electrodeMappingAttr));
            mappingDictionaryName = strsplit(fullAttr, '\');
            mappingDictionaryName = char(mappingDictionaryName(end));
            
            mappingDictionaryFullPath = customElectrodeMappingFileName;
            if isempty(customElectrodeMappingFileName)
                mappingDictionaryFullPath = strcat(obj.mappingDictionariesDir, '/', mappingDictionaryName);
            end
            
            fID = fopen(mappingDictionaryFullPath, 'r');
            dataRead = fscanf(fID, '%d %d', [2, Inf]);
            fclose(fID);
            
            dataReadSizes = size(dataRead);
            for l = 1:dataReadSizes(2)
                dataRead(1, l)
                obj.mappingDictionary(uint32(dataRead(1, l))) = dataRead(2, l);
            end
            
        end
        
        function r = getDatasetsNum(obj)
            %%% GET ONLY NUMBER OF GROUPS - EFFICIENT
            fid = H5F.open(obj.filename);
            gid = H5G.open(fid,'/');
            info = H5G.get_info(gid);
            H5G.close(gid);
            H5F.close(fid);
            
            r = info.nlinks;
        end
    end
    
    methods (Access = public)
        function obj = DVSReader(filename)
            %%%
            obj.filename = filename;
        end
        
        function r = read(obj, timeStart, timeEnd, electrodeStart, electrodeEnd, useElectrodeMappingFile, customElectrodeMappingFileName)
            %%%
            datasetsNum = obj.getDatasetsNum();
            obj.log(strcat('DSET NUM: ', num2str(datasetsNum)))
            
            if datasetsNum == 0
                obj.log('Empty dataset file!')
                r = obj.outputDataDic;
                return
            end
            
            % MATLAB !!!$*$@#@
            obj.mutableDataDic('tStart') = timeStart;
            obj.mutableDataDic('tEnd') = timeEnd;
            
            if useElectrodeMappingFile
                obj.buildElectrodeChannelMapping(strcat(customElectrodeMappingFileName))
            else
                obj.log('Mapping dictionary disabled. Using Default ASIC channel order')
            end
            
            %transform to fractional chunk number
            chunkStartF = timeStart / obj.singleChunkSeconds;
            chunkEndF   = timeEnd / obj.singleChunkSeconds;

            chunkStart  = ceil(chunkStartF);
            chunkEnd    = floor(chunkEndF);

            chunkStartLeft = chunkStartF - chunkStart;
            chunkEndLeft   = chunkEndF   - chunkEnd; 
            
            numChannelsToRead = electrodeEnd - electrodeStart + 1;
            
            %%% HANDLING SOME EXCEPTIONS

            if numChannelsToRead < 1 || timeStart < 0.0 || timeEnd < 0.0 || electrodeStart < 0.0 || electrodeEnd < 0.0
               r = obj.outputDataDic;
               return
            end

            if timeStart >= timeEnd || chunkStartF > datasetsNum
               r = obj.outputDataDic;
               return
            end

            if chunkEndF > datasetsNum   % TRUNCATE
               chunkEndF = datasetsNum;
               chunkEnd = datasetsNum;
               chunkEndLeft = 0;
            end
            
            obj.log(strcat('chunkStartF: ', num2str(chunkStartF)))
            obj.log(strcat('chunkEnd: ', num2str(chunkEndF)))
            
            obj.log(strcat('chunkStart: ', num2str(chunkStart)))
            obj.log(strcat('chunkEnd: ', num2str(chunkEnd)))
            
            obj.log(strcat('chunkStartLeft: ', num2str(chunkStartLeft)))
            obj.log(strcat('chunkEndLeft: ', num2str(chunkEndLeft)))
            
            % MATLAB !!!$*$@#@
            obj.mutableDataDic('totalArraySize') = uint64((abs(chunkStartLeft) + (chunkEnd - chunkStart) + chunkEndLeft) * obj.singleChunkProbes);
            obj.log(strcat('Data probes to read per electrode: ', num2str(obj.mutableDataDic('totalArraySize'))));
            
            for i = electrodeStart:electrodeEnd
                obj.outputDataDic(i) = zeros(1, obj.mutableDataDic('totalArraySize'), 'uint16');
                
                asicChannel = i;
                if isKey(obj.mappingDictionary, i)
                    asicChannel = obj.mappingDictionary(i);
                end
                
                obj.outputDataDic(i) = obj.readASICChannel(asicChannel, chunkStartF, chunkEndF, chunkStart, chunkEnd, chunkStartLeft, chunkEndLeft, obj.outputDataDic(i));
                %obj.log(strcat(num2str(i), '->', num2str(asicChannel)))
            end
        end
        
        function simpleDrawElectrodeData(obj, electrodeID)
           %%%
           if obj.mutableDataDic('totalArraySize') == -1
               obj.log('No data to draw!')
               return
           end
           
           % MATLAB !!!$*$@#@
           s = obj.mutableDataDic('tStart')
           e = double(obj.mutableDataDic('tStart') + double(obj.mutableDataDic('totalArraySize')) / double(obj.singleChunkProbes * 2.0))
           n = double(obj.mutableDataDic('totalArraySize'))
           l = length(obj.outputDataDic(electrodeID))
           
           x = linspace(s, e, n);
           plot(x, obj.outputDataDic(electrodeID))
        end
        
    end
    
end
