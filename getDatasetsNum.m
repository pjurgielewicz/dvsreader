function [totalDatasetsNum, totalTime] = getDatasetsNum(filename)
% filename          - relative path to the HDF data file

% totalDatasetsNum  - amount of chunks in data
% totalTime         - total experiment time

fid = H5F.open(filename);
gid = H5G.open(fid,'/');
info = H5G.get_info(gid);
H5G.close(gid);
H5F.close(fid);

totalDatasetsNum = info.nlinks;
totalTime = totalDatasetsNum * 0.5;