function dataOut = getDatasets(filename, timeStart, timeEnd, channelStart, channelEnd)
% filename       - relative path to the HDF data file
% timeStart [s]  - the moment in time (starting from the beginning of acquisition) when data collecting begins
% timeEnd [s]    - the moment in time (starting from the beginning of acquisition) when data collecting stops
% channelStart   - indexed from 1 #id of the start ASIC channel
% channelEnd     - indexed from 1 #id of the end ASIC channel (inclusive)

% dataOut        - array[timedVal, channel], every 40000 timedVals = 1s, channelStart => 1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONSTANTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

singleChunkSeconds = 0.5;
singleChunkProbes  = 20000;
datasetName        = 'data';

tic
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

numChannelsToRead = channelEnd - channelStart + 1;

% INEFFICIENT
%fileInfo    =    % h5info(filename);
%datasetsNum = length(fileInfo.Groups);

%%% GET ONLY NUMBER OF GROUPS - EFFICIENT
datasetsNum = getDatasetsNum(filename)

%transform to fractional chunk number
chunkStartF = timeStart / singleChunkSeconds;
chunkEndF   = timeEnd / singleChunkSeconds;

chunkStart  = ceil(chunkStartF);
chunkEnd    = floor(chunkEndF);

chunkStartLeft = chunkStartF - chunkStart;
chunkEndLeft   = chunkEndF   - chunkEnd; 

%%% HANDLING SOME EXCEPTIONS

if numChannelsToRead < 1 || timeStart < 0.0 || timeEnd < 0.0 || channelStart < 0.0 || channelEnd < 0.0
   dataOut = [];
   return
end

dataOut = uint16.empty(0, numChannelsToRead);

if timeStart >= timeEnd || chunkStartF > datasetsNum
   return
end

if chunkEndF > datasetsNum   % TRUNCATE
   chunkEndF = datasetsNum;
   chunkEnd = datasetsNum;
   chunkEndLeft = 0;
end

%%% DONE: MAKE MEMORY PREALLOCATION FOR BETTER PERFORMANCE

totalArraySize = uint64((abs(chunkStartLeft) + (chunkEnd - chunkStart) + chunkEndLeft) * singleChunkProbes);
dataOut = zeros(totalArraySize, numChannelsToRead, 'uint16');
currentSaveStartPtr = 1; % MATLAB....

%%% First partial chunk

probesToRead = abs(chunkStartLeft * singleChunkProbes);
if probesToRead > 0
    chunkName = strcat('/', num2str(chunkStart - 1), '/', datasetName);
    r = h5read(filename, chunkName, [singleChunkProbes - probesToRead, channelStart], [probesToRead, numChannelsToRead]);
    
    rLen = length(r);
    dataOut(currentSaveStartPtr : currentSaveStartPtr + rLen - 1, 1:numChannelsToRead) = r;
    currentSaveStartPtr = currentSaveStartPtr + rLen;
    
end

%%% Full (inside) chunks
for ch = chunkStart:chunkEnd-1   
    chunkName = strcat('/', num2str(ch), '/', datasetName);
    r = h5read(filename, chunkName, [1, channelStart], [singleChunkProbes, numChannelsToRead]);
    
    rLen = length(r);
    dataOut(currentSaveStartPtr : currentSaveStartPtr + rLen - 1, 1:numChannelsToRead) = r;
    currentSaveStartPtr = currentSaveStartPtr + rLen;
    
end

%%% Last partial chunk
probesToRead = abs(chunkEndLeft * singleChunkProbes);
if probesToRead > 0 
    if uint64(floor(chunkStartF)) ~= uint64(floor(chunkEndF)) % handle case when start and end fall into the same chunk number
        chunkName = strcat('/', num2str(chunkEnd), '/', datasetName);
        r = h5read(filename, chunkName, [1, channelStart], [probesToRead, numChannelsToRead]);

        rLen = length(r);
        dataOut(currentSaveStartPtr : currentSaveStartPtr + rLen - 1, 1:numChannelsToRead) = r;
        currentSaveStartPtr = currentSaveStartPtr + rLen;
    else
        dataOut = dataOut(1:totalArraySize, 1:numChannelsToRead);
    end
    
end

toc