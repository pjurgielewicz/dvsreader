import matplotlib.pyplot as plt
import numpy as np
import h5py, math, os, getpass, datetime
from scipy import io as sio
from copy import deepcopy

import time, os

class DVSReader:
    def __init__(self, filename, suppressPrintouts = False):
        # filename          - full or relative path to the hdf data file
        
        ############################## CRITICAL CONSTANTS ###############################

        self.dataType               = np.uint16
        
        self.singleChunkSeconds     = 0.5
        self.singleChunkProbes      = 20000
        
        self.datasetName            = 'data'
        
        self.electrodeMappingAttr   = 'Electrode mapping dictionary file'
        self.electrodeMappingDir    = 'data/Electrode Mapping'
        
        self.electrodeLayoutAttr    = 'Electrode layout file'
        self.electrodeLayoutDir     = 'data/Electrode Layout'
        
        self.userNotesAttr          = "User Note"       
        
        self.fileVerAttr            = 'FileVer'

        ############################## VARIABLES #########################################
        
        self.suppressPrintouts      = suppressPrintouts
        
        self.filename               = os.path.abspath(filename)
        self.outputDataDic          = {}
        self.outputTriggerDataDic   = {}
        self.mappingDictionary      = {}
        self.totalArraySize         = -1
        
        self.tStart, self.tEnd      = -1, -1
        
        self.fileVer                = '1.0'                                                 # default input file version
        
        self.electrodeList          = []                                                    # electrode ids to load
        self.triggerChannelList     = [512 + i for i in range(0, 9)]                        # default for ver = 1.0
        self.userNotesList          = []                                                    # list of note dictionaries: chunkNumber, realTime, note
        self.asicConfigDic          = {}                                                    # stores ASIC configuration attributes values
        self.electrodeToChannelDic  = {}                                                    # keeps track of files used to load the right set of electrode data (layout & mapping)
        self.impedanceMeasurementParams = {}
        
        self.dataRate               = int(self.singleChunkProbes / self.singleChunkSeconds) # == frequency
        
        #################################################################################
        
    def __buildChunkName(self, num):
        return ''.join([str(num), '/', self.datasetName])

    def __log(self, *argv):
        if not self.suppressPrintouts:
            print("DVSReader:\t " + str(argv))
        
    def __getAttribute(self, f, attrName, location = '0/data'):
        return f[location].attrs.get(attrName)
        
    def __readUserNoteFromChunk(self, f, currentChunk):
        chunkName = self.__buildChunkName(currentChunk)
        note = self.__getAttribute(f, self.userNotesAttr, chunkName) # f[chunkName].attrs.get(self.userNotesAttr)
        
        if note != None:
            self.userNotesList.append({'chunkNumber' : currentChunk, 'time' : currentChunk * self.singleChunkSeconds, 'note' : note.decode('utf-8')})
    
    def __readUserNotes(self, f, chunkStartF, chunkEndF, chunkStart, chunkEnd, chunkStartLeft, chunkEndLeft):   
        # Can it be optimized ???
        # Left
        if chunkStartLeft != 0.0 and int(math.floor(chunkStartF)) != int(math.floor(chunkEndF)):
            self.__readUserNoteFromChunk(f, chunkStart - 1)
            
        # Middle
        for ch in range(chunkStart, chunkEnd):
            self.__readUserNoteFromChunk(f, ch)
        
        # Right
        if chunkEndLeft != 0.0: # and int(math.floor(chunkStartF)) != int(math.floor(chunkEndF)): ### ???
            self.__readUserNoteFromChunk(f, chunkEnd)
        
        # Exception ???
		
        self.__log(self.userNotesList)
            
        
    def __readASICChannel(self, f, channel, chunkStartF, chunkEndF, chunkStart, chunkEnd, chunkStartLeft, chunkEndLeft, dataOut):
        currentSaveStartPtr = 0; # PYTHON INDEXING

        ### First partial chunk
        probesToRead = int(abs(chunkStartLeft * self.singleChunkProbes))
        if probesToRead > 0 and int(math.floor(chunkStartF)) != int(math.floor(chunkEndF)):
            chunkName = self.__buildChunkName(chunkStart - 1)                                   
            rLen = self.singleChunkProbes - (self.singleChunkProbes - probesToRead)

            newSaveStartPtr = currentSaveStartPtr + rLen
            #read directly to the destination
            f[chunkName].read_direct(dataOut,
                                     source_sel = np.s_[channel : channel + 1, self.singleChunkProbes - probesToRead    : ],
                                     dest_sel   = np.s_[        :            , currentSaveStartPtr                      : newSaveStartPtr])
            currentSaveStartPtr = newSaveStartPtr
               
        ### Full (inside) chunks
        for ch in range(chunkStart, chunkEnd):
            chunkName = self.__buildChunkName(ch)                                              
            
            newSaveStartPtr = currentSaveStartPtr + self.singleChunkProbes
            f[chunkName].read_direct(dataOut,
                                     source_sel = np.s_[channel : channel + 1,                     : ],
                                     dest_sel   = np.s_[        :            , currentSaveStartPtr : newSaveStartPtr])
            currentSaveStartPtr = newSaveStartPtr

        ### Last partial chunk
        probesToRead = int(math.floor(abs(chunkEndLeft * self.singleChunkProbes)))
        if probesToRead > 0 and int(math.floor(chunkStartF)) != int(math.floor(chunkEndF)): # handle case when start and end fall into the same chunk number
            chunkName = self.__buildChunkName(chunkEnd)                                         

            newSaveStartPtr = currentSaveStartPtr + probesToRead
            f[chunkName].read_direct(dataOut,
                                     source_sel = np.s_[channel : channel + 1,                     : probesToRead],
                                     dest_sel   = np.s_[        :            , currentSaveStartPtr : newSaveStartPtr])
            currentSaveStartPtr = newSaveStartPtr
            
        ### Special case - only if start and end are in the same chunk
        if int(math.floor(chunkStartF)) == int(math.floor(chunkEndF)):
            chunkName = self.__buildChunkName(chunkEnd)
            
            sPos = (int(1 + chunkStartLeft * self.singleChunkProbes) + 1) % self.singleChunkProbes
            ePos = int(chunkEndLeft * self.singleChunkProbes)
            #self.__log(sPos, ePos, ePos - sPos, dataOut.shape[1])
            f[chunkName].read_direct(dataOut,
                                     source_sel = np.s_[channel : channel + 1, sPos : ePos],
                                     dest_sel   = np.s_[        :            , 0    : ePos - sPos])
            
        
    def __getFileVersionDependentData(self, f):
        fVer = self.__getAttribute(f, self.fileVerAttr) # f['0/' + self.datasetName].attrs.get(self.fileVerAttr)
        
        if fVer == None:
            self.__log("No < %s > attribute found. Assuming ver: 1.0"%(self.fileVerAttr))
            return
        
        self.fileVer = fVer.decode('utf-8')
        self.__log("File version: ", self.fileVer)
        
        ### SET VERSION-DEPENDENT DATA
        ###
        ### !!!
    
    def __readASICConfig(self, f):
        # ASIC configuration related attributes
        asicConfigAttrs = ['Gain', 'GroundInput', 'High Pass', 'Low Pass',
                           'OffsetCorrection', 'StartFrameLogicLine', 'StimEnable', 'StimGlobalIn', 'StimGlobalOut', 
                           'VoltageFollower', 'chipSelection', 'isHardwareStartFrame']
                           
        for param in asicConfigAttrs:
            attr = self.__getAttribute(f, param)
            
            if attr == None:
                self.__log("No < %s > attribute found. The corresponding information will not be saved."%(param))
                continue
                
            # Update dictionary
            self.asicConfigDic[param] = attr
            
        self.__log(self.asicConfigDic)
        
    def __readImpedanceMeasurementParameters(self, f):
        attrs = {'Impedance Pattern Channels', 'Impedance Pattern Enabled', 'Impedance Pattern Repetition',
                 'DAC 10 bit', 'Current Amplitude [nA]', 'Frequency IDX'}
        
        for param in attrs:
            attr = self.__getAttribute(f, param)
            
            if attr == None:
                self.__log("No < %s > attribute found. The corresponding information will not be saved."%(param))
                continue
              
            if param == 'Impedance Pattern Channels':
                attr = attr.decode('utf-8').split(',')
                attr = [int(s) for s in attr if len(s) > 0]
              
            # Update dictionary
            key = ''.join(param.split(' '))
            self.impedanceMeasurementParams[key] = attr
            
        self.__log(self.impedanceMeasurementParams)
    
    def __readElectrodeLayout(self, f, customElectrodeLayoutFileName):
        electrodeLayoutName = self.__getAttribute(f, self.electrodeLayoutAttr) # f['0/' + self.datasetName].attrs.get(self.electrodeLayoutAttr)
        
        if electrodeLayoutName == None and customElectrodeLayoutFileName == '':
            self.__log("No < %s > attribute found. Relying on user's electrode list to load: "%(self.electrodeLayoutAttr) + str(self.electrodeList))
            return 
            
        electrodeLayoutFullPath = (self.electrodeLayoutDir + '/' + electrodeLayoutName.decode('utf-8').split('\\')[-1]) if customElectrodeLayoutFileName == '' else customElectrodeLayoutFileName
        
        ### FOR CURRENT VERSION ONLY(FULL LABVIEW DEV)
        try:
            elCount = 0
            with open(electrodeLayoutFullPath, 'r') as el:
                for line in el:
                    line = line.strip()
                    if len(line):
                        elCount = elCount + 1
                        
            self.electrodeList = [i + 1 for i in range(0, elCount)]
        except:
            self.__log("Unable to use < %s > file as electrode layout information. Try putting the path of the desired layout explicitly"%(electrodeLayoutFullPath))
            return
            
        self.__log("Using such a list of electrodes based on the electrode layout file: ", self.electrodeList)
        
        return electrodeLayoutFullPath
    
    def __buildElectrodeChannelMapping(self, f, customElectrodeMappingFileName):
        mappingDictionaryName = self.__getAttribute(f, self.electrodeMappingAttr) # f['0/' + self.datasetName].attrs.get(self.electrodeMappingAttr)
        
        if mappingDictionaryName == None and customElectrodeMappingFileName == '':
            self.__log("No < %s > attribute found. All data will be loaded assuming plain ASIC channel order"%(self.electrodeMappingAttr))
            return
        
        mappingDictionaryFullPath = (self.electrodeMappingDir + '/' + mappingDictionaryName.decode('utf-8').split('\\')[-1]) if customElectrodeMappingFileName == '' else customElectrodeMappingFileName

        self.__log("Using: %s file as electrode to ASIC channel dictionary"%(mappingDictionaryFullPath))
        try:
            with open(mappingDictionaryFullPath, 'r') as md:
                for line in md:
                    lineSpl = line.split()
                    
                    # In case there are empty lines
                    if len(lineSpl) != 2:
                        continue
                    
                    # Electrode IDs can be defined as strings (if that's the user's wish...)
                    self.mappingDictionary[ lineSpl[0] ] = int(lineSpl[1])
        except:
            self.__log("Unable to use < %s > file as electrode to ASIC channel dictionary. Try putting the path of the desired dictionary explicitly"%(mappingDictionaryFullPath))
            return
            
        self.__log(self.mappingDictionary)
        
        return mappingDictionaryFullPath
        
    def __readMetadata(self, f, useElectrodeLayoutFile = True, customElectrodeLayoutFileName = '', useElectrodeMappingFile = True, customElectrodeMappingFileName = ''):
        # Set proper version handling
        self.__getFileVersionDependentData(f)
        
        # Get ASIC config information
        self.__readASICConfig(f)
        
        # Get Impedance measurement parameters (if available)
        self.__readImpedanceMeasurementParameters(f)
        
        # Get electrode layout information (if requested)
        electrodeLayoutFileUsed = 'none'
        if useElectrodeLayoutFile:
            electrodeLayoutFileUsed = self.__readElectrodeLayout(f, customElectrodeLayoutFileName)
        else:
            self.__log("Electrode layout information disabled. Using user-prepared electrode list: " + str(self.electrodeList))
        
        # Build electrode -> channel dictionary (if requested)
        electrodeMappingFileUsed = 'none'
        if useElectrodeMappingFile:
            electrodeMappingFileUsed = self.__buildElectrodeChannelMapping(f, customElectrodeMappingFileName)
        else:
            self.__log("Mapping dictionary disabled. Using default ASIC channel order")
            
        self.electrodeToChannelDic = {
                                        'electrodeLayoutFileUsed' : os.path.abspath(electrodeLayoutFileUsed) if electrodeLayoutFileUsed != None else 'none',
                                        'electrodeMappingFileUsed' : os.path.abspath(electrodeMappingFileUsed) if electrodeMappingFileUsed != None else 'none',
                                     }
        
    ########################################################################
        
    def read(self, timeStart, timeEnd, electrodeList, useElectrodeLayoutFile = True, customElectrodeLayoutFileName = '', useElectrodeMappingFile = True, customElectrodeMappingFileName = ''):
        # timeStart [s]         - the moment in time (starting from the beginning of acquisition) when data collecting begins
        # timeEnd [s]           - the moment in time (starting from the beginning of acquisition) when data collecting stops
        # electrodeList         - user-prepared list of electrodes
        
        f = h5py.File(self.filename, 'r')

        # Get num of chunks == is there any usable data?
        datasetsNum = len(f.keys()) 
        self.__log("DSET NUM: " +  str(datasetsNum))

        if datasetsNum == 0:        # If no chunks in file: goodbye
            self.__log("Empty datset file!")
            return
        
        # Set object read principial parameters
        self.tStart, self.tEnd = timeStart, timeEnd
        self.electrodeList = electrodeList
        
        # Remove possible duplicates using easiest approach and...
        self.electrodeList = list(set(self.electrodeList))
        # ...assure that all elements are in ascending order
        self.electrodeList.sort()
        
        # Read input file metadata (! needs to be called after initial user electrode list assignment !)
        self.__readMetadata(f, useElectrodeLayoutFile, customElectrodeLayoutFileName, useElectrodeMappingFile, customElectrodeMappingFileName)
        
        # Transform to fractional chunk number        
        chunkStartF = timeStart / self.singleChunkSeconds
        chunkEndF   = timeEnd   / self.singleChunkSeconds 
        
        chunkStart  = int(math.ceil(chunkStartF))
        chunkEnd    = int(math.floor(chunkEndF))

        chunkStartLeft = chunkStartF - chunkStart
        chunkEndLeft   = chunkEndF   - chunkEnd

        # Since list is sorted use that for quick max & min
        # numChannelsToRead = self.electrodeList[-1] - self.electrodeList[0] + 1
        numChannelsToRead = len(self.electrodeList)
        
        ### HANDLING SOME EXCEPTIONS
        # if numChannelsToRead < 1 or timeStart < 0.0 or timeEnd < 0.0 or self.electrodeList[0] < 0 or self.electrodeList[-1] < 0 or timeStart >= timeEnd or chunkStartF > datasetsNum :
        if numChannelsToRead < 1 or timeStart < 0.0 or timeEnd < 0.0 or timeStart >= timeEnd or chunkStartF > datasetsNum :
           raise ValueError

        if chunkEndF > datasetsNum:   # TRUNCATE
           chunkEndF    = datasetsNum
           chunkEnd     = datasetsNum
           chunkEndLeft = 0
           
        self.__log("chunkStartF: %f"%(chunkStartF))
        self.__log("chunkEndF: %f"%(chunkEndF))        
        
        self.__log("chunkStart: %f"%(chunkStart))
        self.__log("chunkEnd: %f"%(chunkEnd))
        
        self.__log("chunkStartLeft: %f"%(chunkStartLeft))
        self.__log("chunkEndLeft: %f"%(chunkEndLeft))
           
        ### MEMORY PREALLOCATION FOR BETTER PERFORMANCE
        self.totalArraySize = int((abs(chunkStartLeft) + (chunkEnd - chunkStart) + chunkEndLeft) * self.singleChunkProbes)
        self.__log("Data probes to read per electrode: ", self.totalArraySize)
        
        # Based on data length update tEnd to real values 
        self.tEnd = self.tStart + self.totalArraySize / self.dataRate
        
        channelDataArr = np.zeros((1, self.totalArraySize, ), dtype = self.dataType)
        
        # Electrode & Trigger data dictionaries
        self.outputDataDic          = { el : deepcopy(channelDataArr) for el in self.electrodeList}
        self.outputTriggerDataDic   = { el : deepcopy(channelDataArr) for el in self.triggerChannelList}
            
        # Electrodes
        for k in self.outputDataDic:
        
            # If we are using mappingDictionary and the requested id is inside it use the apropriate value, otherwise use just the plain id which is an ASIC channel number
            asicChannel = self.mappingDictionary[k] if k in self.mappingDictionary else k
            
            if isinstance(asicChannel, str) == True:
                self.__log("WARNING!\tElectrode ID: %s does not appear in the mapping dictionary. Skipping..."%( str(asicChannel) ))
                continue
            
            # asicChannel internally starts from 0      
            self.__readASICChannel(f, asicChannel - 1, chunkStartF, chunkEndF, chunkStart, chunkEnd, chunkStartLeft, chunkEndLeft, self.outputDataDic[k])
            
            self.__log(str(k) + " -> " + str(asicChannel), self.outputDataDic[k])
        
        # Triggers (may be MORE than ONE in the future)
        for k in self.outputTriggerDataDic:
        
            try:
                # Assuming no mapping of trigger channels will ever be required
                self.__readASICChannel(f, k - 1, chunkStartF, chunkEndF, chunkStart, chunkEnd, chunkStartLeft, chunkEndLeft, self.outputTriggerDataDic[k])
            
                self.__log("Trigger -> " + str(k), self.outputTriggerDataDic[k])
            except:
                self.__log("Trigger -> " + str(k) + "is not available in the data")
        
        # Load user notes data
        self.__readUserNotes(f, chunkStartF, chunkEndF, chunkStart, chunkEnd, chunkStartLeft, chunkEndLeft)
        
        # Everything done so closing the source file
        f.close()
        
    def simpleDrawElectrodeData(self, electrodeID):
        if self.totalArraySize == -1:
            self.__log("No data to draw!")
            return
        
        if electrodeID not in self.outputDataDic:
            self.__log("No such < %d > electrode ID inside acquired data!"%(electrodeID))
            return
            
        x = np.linspace(self.tStart, self.tEnd, self.totalArraySize)
        
        plt.plot(x, self.outputDataDic[electrodeID][0, :])
        plt.show()
        
    def simpleDrawTriggerData(self, triggerID=512):
        if self.totalArraySize == -1:
            self.__log("No data to draw!")
            return
            
        if triggerID not in self.outputTriggerDataDic:
            self.__log("No such < %d > trigger ID inside acquired data!"%(triggerID))
            return
            
        x = np.linspace(self.tStart, self.tEnd, self.totalArraySize)
        
        plt.plot(x, self.outputTriggerDataDic[triggerID][0, :])
        plt.show()
        
            
    def exportDataToMatlabWorkspace(self, matFilename, compress = False):    
        # Field names can not start with numbers so there should be something added as prefix
        elData          = {"e" + str(el) : self.outputDataDic[el] for el in self.outputDataDic}
        triggerData     = {"t" + str(ch) : self.outputTriggerDataDic[ch] for ch in self.outputTriggerDataDic}
        userNotesData   = self.userNotesList
        metaData        = { 
                            'tStart' : self.tStart, 
                            'tEnd' : self.tEnd,
                            'dataRate' : self.dataRate,
                            'asicConfig' : self.asicConfigDic,
                            'externalFilesUsed' : self.electrodeToChannelDic,
                          } 
        
        d = {   
                'data' : elData, 
                'triggerData' : triggerData, 
                'userNotesData' : userNotesData,
                'metaData' : metaData,
                'impedanceData' : self.impedanceMeasurementParams,
                
                'originalSourceFilePath' : self.filename,
                'creatorComputerName' : os.environ['COMPUTERNAME'],
                'creatorName' : getpass.getuser(),
                'timestamp' : str(datetime.datetime.now())
            }
        
        self.__log(d)
        sio.savemat(matFilename, d, do_compression = compress)
   
########################################################################################################

def exportFiles(inputFilesDir, inputFileNames, outputFilesDir,
                electrodeList,
                timeStart, timeEnd,
                useElectrodeLayoutFile, customElectrodeLayoutFileName,
                useElectrodeMappingFile, customElectrodeMappingFileName,
                useMatlabWorkspaceCompression,
                debug=False
                ):
                
    for fileName in inputFileNames:
    
        fullFileName            = inputFilesDir + fileName
        matlabWorkspaceFileName = outputFilesDir + '.'.join(fileName.split('.')[0:-1]) + '_workspace.mat'
        
        print('\n')
        print('#'*20)
        print('Currently processing: %s'%(fileName))
        print('#'*20)
        print('\n')
        
        try:
            if outputFilesDir == '':
                outputFilesDir = './'
                
            os.makedirs(outputFilesDir, exist_ok=True)
        
            startTime   = time.time()

            dvsObj      = DVSReader(fullFileName)

            dvsObj.read(timeStart, timeEnd, 
                        electrodeList,  
                        useElectrodeLayoutFile, customElectrodeLayoutFileName, 
                        useElectrodeMappingFile, customElectrodeMappingFileName)

            dvsObj.exportDataToMatlabWorkspace(matlabWorkspaceFileName, useMatlabWorkspaceCompression)

            endTime     = time.time()

            print("Elapsed time: %f s"%(endTime - startTime))
            
        except:
            print(">>>\tError while trying to convert %s file !"%(fullFileName))
            print(">>>\tTry reducing the amount of data (time or/and channel count)")
            if debug:
                raise
            continue