function [dataOut, info] = getDatasets(filename, dictionaryFilename, timeStart, timeEnd, electrodeIDs)
% filename              - relative/abs path to the HDF data file
% dictionaryFilename    - relative/abs path of the el2ASIC dictionary file  
% timeStart [s]         - the moment in time (starting from the beginning of acquisition) when data collecting begins
% timeEnd [s]           - the moment in time (starting from the beginning of acquisition) when data collecting stops
% electrodeIDs          - indexed from 1 #ids of the electrode channel

% dataOut               - dictionary (num2str(elID)) = 1D array of samples,, every 40000 samples = 1s
% info                  - info structure storing measurement properties

formatSpec  = "%d %d";
sizeDef     = [2 Inf];

dictionarySize = [0, 0]
dictionaryData = []
dictionaryFilename
if strcmp(dictionaryFilename, '') == 0
    dictionaryFileID = fopen(dictionaryFilename, 'r');

    dictionaryData = fscanf(dictionaryFileID, formatSpec, sizeDef)'
    dictionarySize = size(dictionaryData)
    
    fclose(dictionaryFileID);
end

maxDataChannels = 520;

% Ad-hoc trivial mapping generation
if dictionarySize(1) == 0
    'mapping file is trivial'
    for i=1:maxDataChannels
        tmp = [i i];
        dictionaryData = [dictionaryData; tmp];
    end
    dictionaryData
    dictionarySize = size(dictionaryData);
end

dataOut = containers.Map;
info = h5info(filename, '/0/data');
info = info.Attributes;

% Main loop
for elIdx =1:size(electrodeIDs')
    elID = electrodeIDs(elIdx)
    
    for i=1:dictionarySize(1)
        if elID == dictionaryData(i, 1)
            asicChannel = dictionaryData(i, 2)
            
            dataOut(num2str(elID)) = getHDFDatasets(filename, timeStart, timeEnd, asicChannel, asicChannel);
        end
    end
end


