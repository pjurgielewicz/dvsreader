clear all
clc

% You are responsible for passing paths!
basePath = "E:\Dane\POMIARY\Szczur_2 - Luty 2019\"
filename = strcat(basePath, "LP15_HP01_2019_02_13_15_11_03.dat.0");
dictionaryFilename = "E:\Dane\REPOS\DVSReader\data\Electrode Mapping\samtec40_NeuroNexus2xA32_J1_e_ch.map" %strcat(basePath, "samtec40_NeuroNexus2xA32_J1_e_ch.map");

% Get data length
[num, time] = getDatasetsNum(filename)

timeStart = 0;
timeEnd = 100;

electrodeIDs = [1:20]; % exactly the same numbers (electrodeIDs) as those which were visible during acquisition

[data, info] = getElectrodeData(char(filename), dictionaryFilename, timeStart, timeEnd, electrodeIDs);

figure; hold on
for i = electrodeIDs
    plot(data(num2str(i)))
end

dane    = cell2mat(values(data));
kanaly  = data.keys();